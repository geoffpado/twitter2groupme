package main

import (
	"time"
)

func main() {
	go pollLoop()
	select {}
}

func pollLoop() {
	for {
		time.Sleep(time.Duration(5) * time.Minute)

		tweets := latestTweets()
		latestTweet := tweets[0]
		postTime := latestTweet.PostTime()

		duration := time.Since(postTime)

		if duration.Minutes() <= 5.0 {
			groupmePostText(latestTweet.Text)
		}
	}
}
