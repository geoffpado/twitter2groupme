package main

import (
	"encoding/base64"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type TokenResponse struct {
	TokenType   string `json:"token_type"`
	AccessToken string `json:"access_token"`
}

type Tweet struct {
	PostDate string `json:"created_at"`
	Text     string `json:"text"`
}

func latestTweets() []Tweet {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" + TwitterUsername, nil)
	req.Header.Add("Authorization", "Bearer "+twitterBearerToken())

	resp, err := client.Do(req)
	if err != nil {
		return []Tweet{}
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []Tweet{}
	}

	var tweets []Tweet
	err = json.Unmarshal(body, &tweets)
	if err != nil {
		return []Tweet{}
	}

	return tweets
}

func twitterBearerToken() string {
	client := &http.Client{}
	req, err := http.NewRequest("POST", "https://api.twitter.com/oauth2/token", strings.NewReader("grant_type=client_credentials"))
	req.Header.Add("Authorization", "Basic "+encodedTwitterAuth())
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
	resp, err := client.Do(req)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ""
	}

	//parse the JSON object
	var token TokenResponse
	err = json.Unmarshal(body, &token)
	if err != nil {
		return ""
	}

	return string(token.AccessToken)
}

func encodedTwitterAuth() string {
	escapedTwitterAPIKey := url.QueryEscape(TwitterAPIKey)
	escapedTwitterAPISecret := url.QueryEscape(TwitterAPISecret)

	auth := escapedTwitterAPIKey + ":" + escapedTwitterAPISecret
	encodedAuth := base64.StdEncoding.EncodeToString([]byte(auth))

	return encodedAuth
}

func (tweet Tweet) PostTime() time.Time {
	layout := "Mon Jan 2 15:04:05 -0700 2006"
	postTime, err := time.Parse(layout, tweet.PostDate)
	if err != nil {
		return time.Time{}
	}

	return postTime
}
