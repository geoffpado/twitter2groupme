package main

import (
	"bytes"
	"encoding/json"
	"net/http"
)

type GroupMePost struct {
	BotID    string `json:"bot_id"`
	PostText string `json:"text"`
}

func groupmePostText(text string) {
	post := GroupMePost{BotID: GroupMeBotToken, PostText: text}
	jsonPost, _ := json.Marshal(post)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", "https://api.groupme.com/v3/bots/post", bytes.NewReader(jsonPost))

	resp, _ := client.Do(req)
	defer resp.Body.Close()
}
