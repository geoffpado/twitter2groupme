#Twitter2GroupMe
Twitter2GroupMe allows you to create a GroupMe bot that follows a Twitter user, and posts the latest tweet from that user's timeline.

Twitter2GroupMe currently polls a user timeline every five minutes and posts the latest tweet from that user if it was posted within those five minutes.

##Constants
Twitter2GroupMe requires a file that specifies additional constants for Twitter and GroupMe tokens:

**TwitterAPIKey:** The "API key" from your Twitter app's "API Keys" tab.  
**TwitterAPISecret:** The "API secret" from your Twitter app's "API Keys" tab.  
**TwitterUsername:** The username of the Twitter user that your bot should follow.
**GroupMeBotToken:** The "token" for your GroupMe bot (listed on the [bots page](https://dev.groupme.com/bots))  

##Version History
### v0.2
	- Made Twitter2GroupMe run indefinitely, instead of needing to be set up as a cron job.
	- Added conf variable for the Twitter username that the bot should follow.
### v0.1
	- Initial upload
